#Komoot Notification Service

## Introduction
addThe **Notification Service** is a stand-alone web service for sending Komoot notification. At the moment it only supports welcome notification for new registerd users. It receives a notification from a AWS topic as HTTP POST when a user completes the registration and send a new HTTP POST with the information to send a welcome message to the account. In addition, it expose a HTTP GET to calculate a list of recent users in a given time interval.

## System description

### Architecture
The system is composed of a web server built on **Quarkus** framework ([quarkus.io]()) which implementes the REST endpoints for the AWS subscription and manages the outgoing REST calls for the push notification. The framework is straightforward to setup for development and eases the deploy with docker instance as it comes with preconfigured Docker files. The data side is managed by a MongoDB instance with one table (collection in Mongo terms) where the information about the signups are stored. The choice of these technologies has been taken based on the ease of setup, use and deploy with Docker in relation  with the functionalities they offer for the objective of this work.

### Functionalities
#### New user listener
A REST endpoint receives a payload with user data (id, name and creation date) and simply and saves those information in MongoDB. 

#### Recent user calculation 
The calculation of a list of recent users based on a date interval to filter the registration date, and the maximum number of users to return. The list will be ordered based on the creation date in a descending fashion so that the users contained in it will always be the last ones registered.

#### New registration listener and notifier
A scheduled action from Quarkus execute every 5 seconds a set of commands to query from MongoDB table the users who have not yet received a (welcome) notification. For each returned record, a list of recent users is calculated (see previous section) and a payload is created with the following information: the sender email, the ID of the user to notify, the message and the IDs of the recent users returned from the calculation. To obtain such a list, the system takes all the users who have done the signup up to 10 days before (configurable) the registration date of the current user to notify and extract the three most recent ones (configurable). In case no new registration have been received in the last 10 days or no user is present in the system, the message will be constructed accordingly.

## Deploy on remote server
The orchestration of the web server and MongoDB instances is managed by a *docker-compose* configuration. To deploy the application on a server, one need to copy to the remote machine the folders `config` and `docker` (except the `docker-compose.override.yml` file). Then, the variables in the `.env` files of the `docker` folder must be correctly set:

1.`SERVER_IMAGE_TAG`: the tag of the docker image to use (you can leave the one already present)
 
2.`CONFIG_HOST_PATH`: the absolute path on the remote machine where the `config.yml` file has been copied

3.`CONFIG_PATH`: the absolute path to which the `config.yml` file is mounted on the docker container of the web server

Number 1. and 3. can be left as they are, while number 2. must be set with the right value of the path on the remote machine.
To run the system, one should position on the `docker` folder on the remote server and execute the command `docker-compose --env-file .env up -d`. If the name of the containers is shown with no error, the system is up and running correctly.

**N.B.:** 
- in order to allow external traffic to the web server, the port `7080` must be open on the remote machine
- the remote machine **must** have docker installed 

## Examples REST API

### Push a new registerd users
``POST /rest/api/notification/aws/push-endpoint``

```json
{
    "name": "Marcus",
    "id": 1589278470,
    "created_at": "2020-05-12T16:11:54.000"
}
```

### Get the list of recent users
``GET /rest/api/notification/recent-users``

Query parameters examples
```
max_users=4 -> max user to return
start_date=2022-04-05T12%3A00%3A00%2B02 -> the interval start for the registration date
end_date=2022-06-10T12%3A00%3A00%2B02 -> the interval end for the registration date
```

[comment]: <> (## Future improvements)

[comment]: <> (- generalization of notification via configuration)

[comment]: <> (- add parameters to limit the set of users for the welcome)

[comment]: <> (- add pipeline to automatize deploy + tests )
