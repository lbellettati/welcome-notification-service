package com.komoot.logic;

public abstract class Action {

    protected String name;

    public Action() {
    }

    public Action(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract void execute();
}
