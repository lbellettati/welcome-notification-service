package com.komoot.logic;

import com.komoot.entities.SignedUpNotificationEntity;
import com.komoot.mongo.MongoUtility;
import com.komoot.mongo.query.UserToGreetQuery;
import io.quarkus.scheduler.Scheduled;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.logging.Logger;

@ApplicationScoped
public class SignUpListener {

    private static final Logger LOG = Logger.getLogger(SignUpListener.class.getName());

    private static boolean running = false;

    @Inject
    NewUserGreeting greetingInstance;

    @Scheduled(every = "2s")
    public void listen() {
        if (running) {
            LOG.info("Listening job already running (SKIP)");
            return;
        }
        try {
            running = true;
            LOG.info("Checking for users to greet");
            List<SignedUpNotificationEntity> usersToGreet = MongoUtility.executeFindQuery(new UserToGreetQuery(), SignedUpNotificationEntity.class);

            for (SignedUpNotificationEntity toGreet: usersToGreet) {
                // set the user to greet
                this.greetingInstance.setUserToGreet(toGreet);

                // send greeting notification
                this.greetingInstance.execute();

                // set the signup as greeted
                toGreet.setGreeted(true);
                MongoUtility.update(toGreet);

                // break loop to let the query result be up to date
                if (usersToGreet.size() > 1) {
                    break;
                }
            }
        } catch (Exception ex) {
            LOG.severe(ex.getMessage());
            ex.printStackTrace();
        } finally {
            running = false;
        }
    }
}
