package com.komoot.logic;

import com.komoot.entities.SignedUpNotificationEntity;
import com.komoot.mongo.MongoUtility;
import com.komoot.mongo.query.SignedUpUsersQuery;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class RecentUserCalculator {
    private Date startDate;
    private List<SignedUpNotificationEntity> userSignupList;
    private int maxUsers = -1;
    private Date endDate;

    public RecentUserCalculator(List<SignedUpNotificationEntity> userSignupList) {
        this.userSignupList = userSignupList;
        this.maxUsers = -1;
        this.endDate = getDefaultPastDateLimit();
    }

    public RecentUserCalculator(List<SignedUpNotificationEntity> userSignupList, int maxUsers) {
        this.userSignupList = userSignupList;
        this.maxUsers = maxUsers;
        this.endDate = getDefaultPastDateLimit();
    }

    public RecentUserCalculator(List<SignedUpNotificationEntity> userSignupList, int maxUsers, Date endDate) {
        this.userSignupList = userSignupList;
        this.maxUsers = maxUsers;
        this.endDate = endDate == null ? getDefaultPastDateLimit() : endDate;
    }

    public RecentUserCalculator(Date startDate, List<SignedUpNotificationEntity> userSignupList, int maxUsers, Date endDate) {
        this.startDate = startDate;
        this.userSignupList = userSignupList;
        this.maxUsers = maxUsers;
        this.endDate = endDate;
    }

    public List<SignedUpNotificationEntity> getRecentUsers() {
        // init the result
        List<SignedUpNotificationEntity> recentUsers = new ArrayList<>();

        if (this.userSignupList == null || this.userSignupList.isEmpty()) {
            return recentUsers;
        }

        // iterate on user appearences
        for (SignedUpNotificationEntity userAppearence: this.userSignupList) {
            SignedUpNotificationEntity signupToCheck = checkRecentUser(userAppearence.getUserId(), this.startDate, this.endDate);
            if (signupToCheck != null && !recentUsers.contains(signupToCheck)) {
                recentUsers.add(signupToCheck);
            }

            // stop if the max users to show has been reached
            if (maxUsers != -1 && recentUsers.size() == maxUsers) {
                break;
            }
        }

        return recentUsers;
    }

    public static SignedUpNotificationEntity checkRecentUser(int userId, Date startDate, Date endDate) {
        List<SignedUpNotificationEntity> users = MongoUtility.executeFindQuery(new SignedUpUsersQuery(userId), SignedUpNotificationEntity.class);

        if (users != null && !users.isEmpty()) {
            return checkRecentUser(users.get(0), startDate, endDate);
        }
        return null;
    }

    public static SignedUpNotificationEntity checkRecentUser(SignedUpNotificationEntity signup, Date startDate, Date endDate) {
        boolean startDateOk = startDate != null ? signup.getCreatedAt().after(startDate) : true;
        boolean endDateOk = endDate != null ? signup.getCreatedAt().before(endDate) : true;

        if (startDateOk && endDateOk) {
            return signup;
        }
        return null;
    }

    public int getMaxUsers() {
        return maxUsers;
    }

    public void setMaxUsers(int maxUsers) {
        this.maxUsers = maxUsers;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    private Date getDefaultPastDateLimit() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.YEAR, 1);
        return c.getTime();
    }
}
