package com.komoot.logic;

import com.komoot.configuration.Configuration;
import com.komoot.configuration.MongoDBConnection;
import com.komoot.entities.SignedUpNotificationEntity;
import com.komoot.entities.UserAppearence;
import com.komoot.entities.WelcomeNotificationEntity;
import com.komoot.mongo.MongoUtility;
import com.komoot.mongo.query.RecentGreetingsQuery;
import com.komoot.mongo.query.SignedUpUsersQuery;
import com.komoot.rest.client.WelcomeNotificationService;
import com.mongodb.client.ClientSession;
import com.mongodb.client.MongoClient;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

@ApplicationScoped
public class NewUserGreeting extends Action {

    private static final Logger LOG = Logger.getLogger(NewUserGreeting.class.getName());

    private static String MESSAGE_GREET = "Hi %s, welcome to komoot. ";
    private static String FIRST_USER_GREET = "You are the first user of the app!";
    private static String LONG_TIME_NO_USERS_GREET = "You are the first new user in the system since %d day(s)!";
    private static String LAST_USER_GREET = " and %s";
    private static String JOINED_MESSAGE = " also joined recently.";

    private SignedUpNotificationEntity userToGreet;

    @Inject
    @RestClient
    WelcomeNotificationService welcomeNotificationService;

    public NewUserGreeting() {
        this.name = "NewUserGreetingAction";
    }

    public NewUserGreeting(SignedUpNotificationEntity userToGreet) {
        this.userToGreet = userToGreet;
        this.name = "NewUserGreetingAction";
    }

    public NewUserGreeting(String name, SignedUpNotificationEntity userToGreet) {
        super(name);
        this.userToGreet = userToGreet;
    }

    public SignedUpNotificationEntity getUserToGreet() {
        return userToGreet;
    }

    public void setUserToGreet(SignedUpNotificationEntity userToGreet) {
        this.userToGreet = userToGreet;
    }

    public WelcomeNotificationService getWelcomeNotificationService() {
        return welcomeNotificationService;
    }

    public void setWelcomeNotificationService(WelcomeNotificationService welcomeNotificationService) {
        this.welcomeNotificationService = welcomeNotificationService;
    }

    @Override
    public void execute() {
        // calculate the recent recente users
        List<SignedUpNotificationEntity> greetingUsers = new RecentUserCalculator(
                getDateLimitFromDate(this.userToGreet.getCreatedAt(), Configuration.get().getMaxNewDays()),
                MongoUtility.executeFindQuery(new RecentGreetingsQuery(this.userToGreet.getCreatedAt(), this.userToGreet.getUserId()), SignedUpNotificationEntity.class),
                Configuration.get().getMaxNewUsersShown(),
                this.userToGreet.getCreatedAt()
            ).getRecentUsers();

        List<String> userNames = new ArrayList<>();
        List<Integer> userIds = new ArrayList<>();

        // get users names and ids for recent users
        for (SignedUpNotificationEntity greetingUser: greetingUsers) {
            userNames.add(greetingUser.getName());
            userIds.add(greetingUser.getUserId());
        }

        // create the paylod for the welcome message
        WelcomeNotificationEntity welcomeNotificationEntity = getGreetingPayload(userNames, userIds);

        LOG.info(String.format("Welcome message: %s", welcomeNotificationEntity.getMessage()));

        // get the mongo connection from the configuration
        MongoDBConnection mongoConn = Configuration.get().getMongo();

        // setup the mongo connection with transaction
        MongoClient mongoClient = MongoUtility.getClient(mongoConn);
        ClientSession session = mongoClient.startSession();

        String response = null;
        try {
            // start mongo db transaction
            session.startTransaction();

            // send the welcome message as push notification
            response = welcomeNotificationService.postWelcomeNotification(welcomeNotificationEntity);

            if (response != null &&! response.isEmpty()) {
                throw new Exception(String.format("Error while sending welcome notification: %s", response));
            }

            // set the user as greeted
            userToGreet.setGreeted(true);
            MongoUtility.update(mongoClient, userToGreet);

            // add appearence for greeting users
            for (Integer uid: userIds) {
                MongoUtility.save(mongoClient, new UserAppearence(uid, new Date()), Configuration.get().getMongo().getCollections().get("greeting"));
            }

            // commit transaction
            session.commitTransaction();
        } catch (Exception e) {
            // rollback query and print the error
            session.abortTransaction();
            System.out.printf("Errore while pushing welcome message: %s", e.getMessage());
        } finally {
            session.close();
        }
    }

    /**
     *
     * @param userNames the list of the greeting users
     * @param userIds the ids of the greeting users
     * @return a {@link com.komoot.entities.WelcomeNotificationEntity} instance
     */
    public WelcomeNotificationEntity getGreetingPayload(List<String> userNames, List<Integer> userIds) {
        WelcomeNotificationEntity welcomeNotificationEntity = new WelcomeNotificationEntity();
        welcomeNotificationEntity.setReceiver(userToGreet.getUserId());
        welcomeNotificationEntity.setRecentUserIds(userIds);

        boolean isSignupsEmpty = MongoUtility.executeFindQuery(new SignedUpUsersQuery(), SignedUpNotificationEntity.class).isEmpty();
        welcomeNotificationEntity.setMessage(getGreetingMessage(userNames, isSignupsEmpty));

        return welcomeNotificationEntity;
    }

    /**
     *
     * @param userNames the list of user names for the greeting message
     * @return String the message to use to greet
     */
    public String getGreetingMessage(List<String> userNames, boolean empty) {
        String message = "";
        message += String.format(MESSAGE_GREET, this.userToGreet.getName());

        if (empty) {
            message += FIRST_USER_GREET;
            return message;
        }

        if ((userNames == null || userNames.isEmpty()) && !empty) {
            message += String.format(LONG_TIME_NO_USERS_GREET, Configuration.get().getMaxNewDays());
            return message;
        }

        int userNamesCount = userNames.size();

        if (userNamesCount == 1) {
            message += String.format("%s%s", userNames.get(userNamesCount - 1), JOINED_MESSAGE);
            return message;
        }

        if (userNamesCount > 1) {
            String firstUserName = userNames.get(0);
            String lastUserName = userNames.get(userNamesCount - 1);
            message += firstUserName;
            for (int i = 1; i < userNamesCount - 1; i++) {
                message += String.format(", %s", userNames.get(i));
            }
            message += String.format(LAST_USER_GREET, lastUserName);
            message += JOINED_MESSAGE;
        }

        return message;
    }

    private Date getDateLimit(int maxNewDays) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, -maxNewDays);
        return c.getTime();
    }

    private Date getDateLimitFromDate(Date date, int maxNewDays) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DAY_OF_MONTH, -maxNewDays);
        return c.getTime();
    }
}
