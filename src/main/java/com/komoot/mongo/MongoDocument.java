package com.komoot.mongo;

import org.bson.Document;
import org.bson.conversions.Bson;

public interface MongoDocument {
    Document toDocument();
    Bson buildUpdateCondition();
    String getCollection();
}
