package com.komoot.mongo;

public interface MongoQuery {
    String getCollection();
}
