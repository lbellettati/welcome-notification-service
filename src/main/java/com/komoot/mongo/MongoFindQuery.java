package com.komoot.mongo;

import org.bson.conversions.Bson;

public interface MongoFindQuery<T extends MongoDocument> extends MongoQuery{
    Bson filter();
    Bson project();
    Bson orderBy();
    Class<T> getMappingClass();
}
