package com.komoot.mongo.query;

import com.komoot.configuration.Configuration;
import com.komoot.entities.SignedUpNotificationEntity;
import com.komoot.mongo.MongoFindQuery;
import org.bson.conversions.Bson;

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Sorts.descending;
import static com.mongodb.client.model.Projections.include;

public class SignedUpUsersQuery implements MongoFindQuery<SignedUpNotificationEntity> {
    private int userId = -1;
    private boolean greeted = true;

    public SignedUpUsersQuery() {
    }

    public SignedUpUsersQuery(int userId) {
        this.userId = userId;
    }

    public SignedUpUsersQuery(int userId, boolean greeted) {
        this.userId = userId;
        this.greeted = greeted;
    }

    @Override
    public Bson filter() {
        if (userId != -1) {
            return eq("user_id", userId);
        }
        return empty();
    }

    @Override
    public Bson project() {
        return include();
    }

    @Override
    public Bson orderBy() {
        return descending("created_at");
    }

    @Override
    public String getCollection() {
        return Configuration.get().getMongo().getCollections().get("signup");
    }

    @Override
    public Class<SignedUpNotificationEntity> getMappingClass() {
        return SignedUpNotificationEntity.class;
    }
}
