package com.komoot.mongo.query;

import com.komoot.configuration.Configuration;
import com.komoot.entities.SignedUpNotificationEntity;
import com.komoot.mongo.MongoFindQuery;
import org.bson.conversions.Bson;

import java.util.Date;

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Projections.include;
import static com.mongodb.client.model.Sorts.descending;

public class RecentGreetingsQuery implements MongoFindQuery<SignedUpNotificationEntity> {
    private Date endDate;
    private int userToGreetId;

    public RecentGreetingsQuery() {
        this.endDate = new Date();
        this.userToGreetId = -1;
    }

    public RecentGreetingsQuery(Date endDate) {
        this.endDate = endDate;
        this.userToGreetId = -1;
    }

    public RecentGreetingsQuery(Date endDate, int userToGreetId) {
        this.endDate = endDate;
        this.userToGreetId = userToGreetId;
    }

    @Override
    public Bson filter() {
        return and(lt("created_at", endDate), ne("user_id", this.userToGreetId));
    }

    @Override
    public Bson project() {
        return include();
    }

    @Override
    public Bson orderBy() {
        return descending("created_at");
    }

    @Override
    public String getCollection() {
        return Configuration.get().getMongo().getCollections().get("signup");
    }

    @Override
    public Class<SignedUpNotificationEntity> getMappingClass() {
        return SignedUpNotificationEntity.class;
    }
}
