package com.komoot.mongo.query;

import com.komoot.configuration.Configuration;
import com.komoot.entities.SignedUpNotificationEntity;
import com.komoot.mongo.MongoFindQuery;
import org.bson.conversions.Bson;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Projections.include;
import static com.mongodb.client.model.Sorts.ascending;

public class UserToGreetQuery implements MongoFindQuery<SignedUpNotificationEntity> {
    @Override
    public Bson filter() {
        return eq("greeted", false);
    }

    @Override
    public Bson project() {
        return include();
    }

    @Override
    public Bson orderBy() {
        return ascending("created_at");
    }

    @Override
    public Class<SignedUpNotificationEntity> getMappingClass() {
        return SignedUpNotificationEntity.class;
    }

    @Override
    public String getCollection() {
        return Configuration.get().getMongo().getCollections().get("signup");
    }
}
