package com.komoot.mongo;

import com.komoot.configuration.Configuration;
import com.komoot.configuration.MongoDBConnection;
import com.mongodb.MongoException;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.codecs.configuration.CodecProvider;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import java.util.ArrayList;
import java.util.List;

import static com.mongodb.MongoClientSettings.getDefaultCodecRegistry;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class MongoUtility {
    public static MongoClient getClient(MongoDBConnection db) {
        String connectionString = String.format("mongodb://%s:%s",db.getHost(), db.getPort());
        return MongoClients.create(connectionString);
    }

    public static MongoDatabase getMongoDB(String dbName, MongoDBConnection db) {
        return getClient(db).getDatabase(dbName);
    }

    public static void save(MongoDocument entry, String collectionName) {
        MongoDBConnection mongoConn = Configuration.get().getMongo();

        try (MongoClient mongoClient = MongoUtility.getClient(mongoConn)) {
            save(mongoClient, entry, collectionName);
        } catch (MongoException me) {
            throw (me);
        }
    }

    public static void save(MongoClient mongoClient, MongoDocument entry, String collectionName) {
        MongoDBConnection mongoConn = Configuration.get().getMongo();

        try {
            MongoDatabase database = mongoClient.getDatabase(mongoConn.getDbName());
            MongoCollection<Document> collection = database.getCollection(collectionName);
            collection.insertOne(entry.toDocument());
        } catch (MongoException me) {
            throw (me);
        }
    }

    public static void update(MongoDocument entry) {
        CodecProvider pojoCodecProvider = PojoCodecProvider.builder().register(entry.getClass()).build();
        CodecRegistry pojoCodecRegistry = fromRegistries(getDefaultCodecRegistry(), fromProviders(pojoCodecProvider));

        MongoDBConnection mongoConn = Configuration.get().getMongo();

        try (MongoClient mongoClient = MongoUtility.getClient(mongoConn)) {
            update(mongoClient, entry);
        } catch (MongoException me) {
            throw(me);
        }
    }

    public static void update(MongoClient mongoClient, MongoDocument entry) {
        CodecProvider pojoCodecProvider = PojoCodecProvider.builder().register(entry.getClass()).build();
        CodecRegistry pojoCodecRegistry = fromRegistries(getDefaultCodecRegistry(), fromProviders(pojoCodecProvider));

        MongoDBConnection mongoConn = Configuration.get().getMongo();

        try {
            MongoDatabase database = mongoClient.getDatabase(mongoConn.getDbName()).withCodecRegistry(pojoCodecRegistry);
            MongoCollection<Document> collection = database.getCollection(entry.getCollection());
            collection.updateOne(entry.buildUpdateCondition(), new Document("$set", entry.toDocument()));
        } catch (MongoException me) {
            throw(me);
        }
    }

    public static <T extends MongoDocument> List<T> executeFindQuery(MongoFindQuery query, Class<T> mappingClass) {
        CodecProvider pojoCodecProvider = PojoCodecProvider.builder().register(mappingClass).build();
        CodecRegistry pojoCodecRegistry = fromRegistries(getDefaultCodecRegistry(), fromProviders(pojoCodecProvider));

        // get the mongo connection from the configuration
        MongoDBConnection mongoConn = Configuration.get().getMongo();

        // perform the query
        try (MongoClient mongoClient = MongoUtility.getClient(mongoConn)) {
            return executeFindQuery(mongoClient, query, mappingClass);
        } catch (MongoException me) {
            throw(me);
        }
    }

    public static <T extends MongoDocument> List<T> executeFindQuery(MongoClient mongoClient, MongoFindQuery query, Class<T> mappingClass) {
        CodecProvider pojoCodecProvider = PojoCodecProvider.builder().register(mappingClass).build();
        CodecRegistry pojoCodecRegistry = fromRegistries(getDefaultCodecRegistry(), fromProviders(pojoCodecProvider));

        // get the mongo connection from the configuration
        MongoDBConnection mongoConn = Configuration.get().getMongo();

        // init the result
        List<T> result = new ArrayList<>();

        // perform the query
        try {
            // get the database
            MongoDatabase database = mongoClient.getDatabase(mongoConn.getDbName()).withCodecRegistry(pojoCodecRegistry);

            // get the collection
            MongoCollection<T> collection = database.getCollection(query.getCollection(), query.getMappingClass());

            // execute the query
            collection.find(query.filter()).sort(query.orderBy()).projection(query.project()).into(result);
        } catch (MongoException me) {
            throw(me);
        }

        return result;
    }

}
