package com.komoot;

import com.komoot.configuration.Configuration;
import com.komoot.configuration.MongoDBConnection;
import com.komoot.mongo.MongoUtility;
import com.mongodb.client.MongoClient;
import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.annotations.QuarkusMain;

import javax.enterprise.context.ApplicationScoped;
import java.util.logging.Level;

@QuarkusMain
@ApplicationScoped
public class NotificationServiceMain {

    public static void main(String[] args) {
        run(args);
    }

    public static void run(String[] a) {
        // set log level for mongo
        java.util.logging.Logger.getLogger("org.mongodb.driver").setLevel(Level.SEVERE);

        // init mongo client
        MongoClient mongoClient = null;
        try {
            // get the mongo connection from configuration
            MongoDBConnection mongoDBConnection = Configuration.get().getMongo();

            // build client
            mongoClient = MongoUtility.getClient(mongoDBConnection);

            // try list the databases to see if the connection is UP
            mongoClient.listDatabases();
        } catch (Exception e) {
            System.out.println("MongoDB Server is Down");
            System.out.println("ABORTING startup");

            // closing mongo client if not null
            if (mongoClient != null) {
                mongoClient.close();
            }

            // abort startup
            return;
        }

        // start the web server
        Quarkus.run(a);
    }
}
