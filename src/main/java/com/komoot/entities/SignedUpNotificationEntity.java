package com.komoot.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.komoot.configuration.Configuration;
import com.komoot.mongo.MongoDocument;
import org.bson.Document;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.conversions.Bson;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

import static com.mongodb.client.model.Filters.eq;


public class SignedUpNotificationEntity implements MongoDocument {

    @JsonProperty(required = true)
    @NotBlank (message = "name cannot be blank")
    private String name;

    @NotNull(message = "id must be greater than 0")
    @JsonProperty(value = "id",required = true)
    @BsonProperty("user_id")
    private Integer userId;

    @JsonProperty(value = "created_at",required = true)
    @NotNull (message = "created_at cannot be blank")
    @BsonProperty("created_at")
    private Date createdAt;

    private boolean greeted = false;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public boolean isGreeted() {
        return greeted;
    }

    public void setGreeted(boolean greeted) {
        this.greeted = greeted;
    }

    @Override
    public Document toDocument() {
        return new Document()
                .append("name", this.name)
                .append("user_id", this.userId)
                .append("created_at", this.createdAt)
                .append("greeted", this.greeted);
    }

    @Override
    public Bson buildUpdateCondition() {
        return eq("user_id", this.userId);
    }

    @Override
    public String getCollection() {
        return Configuration.get().getMongo().getCollections().get("signup");
    }

    @Override
    public String toString() {
        return String.format("name= %s, id=%d, created_at=%s", name, userId, createdAt);
    }
}
