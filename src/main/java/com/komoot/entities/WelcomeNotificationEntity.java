package com.komoot.entities;

import java.util.List;

public class WelcomeNotificationEntity {
    private String sender;
    private int receiver;
    private String message;
    private List<Integer> recentUserIds;

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public int getReceiver() {
        return receiver;
    }

    public void setReceiver(int receiver) {
        this.receiver = receiver;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Integer> getRecentUserIds() {
        return recentUserIds;
    }

    public void setRecentUserIds(List<Integer> recentUserIds) {
        this.recentUserIds = recentUserIds;
    }
}
