package com.komoot.entities;

import com.komoot.configuration.Configuration;
import com.komoot.mongo.MongoDocument;
import org.bson.Document;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.conversions.Bson;

import java.util.Date;

public class UserAppearence implements MongoDocument {
    @BsonProperty(value = "user_id")
    private int userId;

    @BsonProperty(value = "timestamp")
    private Date appearenceTimestamp;

    public UserAppearence() {
    }

    public UserAppearence(int userId, Date appearenceTimestamp) {
        this.userId = userId;
        this.appearenceTimestamp = appearenceTimestamp;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Date getAppearenceTimestamp() {
        return appearenceTimestamp;
    }

    public void setAppearenceTimestamp(Date appearenceTimestamp) {
        this.appearenceTimestamp = appearenceTimestamp;
    }

    @Override
    public Document toDocument() {
        return new Document()
                .append("user_id", userId)
                .append("timestamp", appearenceTimestamp);
    }

    @Override
    public Bson buildUpdateCondition() {
        return null;
    }

    @Override
    public String getCollection() {
        return Configuration.get().getMongo().getCollections().get("greeting");
    }
}
