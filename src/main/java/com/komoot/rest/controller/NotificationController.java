package com.komoot.rest.controller;

import com.komoot.configuration.Configuration;
import com.komoot.entities.SignedUpNotificationEntity;
import com.komoot.entities.WelcomeNotificationEntity;
import com.komoot.logic.RecentUserCalculator;
import com.komoot.logic.SignUpListener;
import com.komoot.mongo.MongoUtility;
import com.komoot.mongo.query.RecentGreetingsQuery;

import javax.enterprise.context.ApplicationScoped;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

@ApplicationScoped
@Path("/rest/api/notification")
public class NotificationController {

    private static final Logger LOG = Logger.getLogger(SignUpListener.class.getName());

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/hello")
    public String hello() {
        return "Hello, this is Komoot Notification Service!";
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/aws/push-endpoint")
    public Response signupNotificationListener(@Valid @NotNull(message = "input cannot be blank") SignedUpNotificationEntity input) {
        LOG.info(String.format("Received notification: %s", input.toString()));
        try {
            // save newly signed up user
            MongoUtility.save(input, Configuration.get().getMongo().getCollections().get("signup"));
        } catch (Exception e) {
            LOG.severe(e.getMessage());
            e.printStackTrace();

            // send error response in case of exception
            return Response.serverError().entity(e.getMessage()).build();
        }

        // return ok response
        return Response.ok().build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/recent-users")
    public Response getRecentUsers(@QueryParam("max_users") Integer maxUsers, @QueryParam("start_date") String startDateString, @QueryParam("end_date") String endDateString) {
        Date startDate = null, endDate = null;
        try {
            if (startDateString != null) {
                startDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX").parse(startDateString);
            }
            if (endDateString != null) {
                endDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX").parse(endDateString);
            }
        } catch (Exception ex) {
            return Response.serverError().entity(ex.getMessage()).build();
        }

        return Response.ok(new RecentUserCalculator(
                startDate,
                MongoUtility.executeFindQuery(new RecentGreetingsQuery(), SignedUpNotificationEntity.class),
                maxUsers == null ? -1 : maxUsers,
                endDate).getRecentUsers()
            ).build();
    }

    @POST
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/push-welcome")
    public Response pushWelcomeTest(WelcomeNotificationEntity welcomeNotificationEntity) {
        return Response.ok(welcomeNotificationEntity).build();
    }
}