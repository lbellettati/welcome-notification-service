package com.komoot.rest.client;

import com.komoot.entities.WelcomeNotificationEntity;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

@Path("/")
@RegisterRestClient(configKey = "welcome-notification-api")
@ApplicationScoped
public interface WelcomeNotificationService {

    @POST
    String postWelcomeNotification(WelcomeNotificationEntity entity);
}
