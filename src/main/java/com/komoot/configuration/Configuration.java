package com.komoot.configuration;

import org.yaml.snakeyaml.TypeDescription;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.Path;

public class Configuration {

    private MongoDBConnection mongo;

    private Integer maxNewDays;

    private Integer maxNewUsersShown;

    private String notificationSender;

    public MongoDBConnection getMongo() {
        return mongo;
    }

    public void setMongo(MongoDBConnection mongo) {
        this.mongo = mongo;
    }

    public Integer getMaxNewDays() {
        return maxNewDays;
    }

    public void setMaxNewDays(Integer maxNewDays) {
        this.maxNewDays = maxNewDays;
    }

    public Integer getMaxNewUsersShown() {
        return maxNewUsersShown;
    }

    public void setMaxNewUsersShown(Integer maxNewUsersShown) {
        this.maxNewUsersShown = maxNewUsersShown;
    }

    public String getNotificationSender() {
        return notificationSender;
    }

    public void setNotificationSender(String notificationSender) {
        this.notificationSender = notificationSender;
    }

    public static Configuration get() {
        Path p = Path.of(System.getenv("CONFIG_PATH"));
        try {
            return new Yaml(getYAMLConstructor()).load(new FileInputStream(p.toString()));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Constructor getYAMLConstructor() {
        Constructor constructor = new Constructor(Configuration.class);
        TypeDescription td = new TypeDescription(MongoDBConnection.class);
        td.substituteProperty("dbname", String.class, "getDbName", "setDbName");
        constructor.addTypeDescription(td);
        td = new TypeDescription(Configuration.class);
        td.substituteProperty("max_new_days", Integer.class, "getMaxNewDays", "setMaxNewDays");
        td.substituteProperty("max_new_users_shown", Integer.class, "getMaxNewUsersShown", "setMaxNewUsersShown");
        td.substituteProperty("notification_sender", String.class, "getNotificationSender", "setNotificationSender");
        constructor.addTypeDescription(td);
        return constructor;
    }
}

