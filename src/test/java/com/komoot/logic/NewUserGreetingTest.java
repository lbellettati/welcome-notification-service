package com.komoot.logic;

import com.komoot.entities.SignedUpNotificationEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class NewUserGreetingTest {

    @BeforeEach
    void setUp() {
    }

    @Test
    void getGreetingMessage() {
        SignedUpNotificationEntity signedUpNotificationEntity = new SignedUpNotificationEntity();
        signedUpNotificationEntity.setName("Luca");
        signedUpNotificationEntity.setUserId(123456789);
        signedUpNotificationEntity.setCreatedAt(new Date());

        NewUserGreeting newUserGreeting = new NewUserGreeting(signedUpNotificationEntity);

        // 1 existing user
        List<String> userNames = new ArrayList<>() {{
            add("User1");
        }};
        String greetingMessage = newUserGreeting.getGreetingMessage(userNames, false);
        assertEquals("Hi Luca, welcome to komoot. User1 also joined recently.", greetingMessage);

        // 2 existing users
        userNames = new ArrayList<>() {{
            add("User1");
            add("User2");
        }};
        greetingMessage = newUserGreeting.getGreetingMessage(userNames, false);
        assertEquals("Hi Luca, welcome to komoot. User1 and User2 also joined recently.", greetingMessage);

        // first user
        userNames = new ArrayList<>();
        greetingMessage = newUserGreeting.getGreetingMessage(userNames, true);
        assertEquals("Hi Luca, welcome to komoot. You are the first user of the app!", greetingMessage);

        // first user since long time
        userNames = new ArrayList<>();
        greetingMessage = newUserGreeting.getGreetingMessage(userNames, false);
        assertEquals("Hi Luca, welcome to komoot. You are the first new user in the system since 10 day(s)!", greetingMessage);
    }
}