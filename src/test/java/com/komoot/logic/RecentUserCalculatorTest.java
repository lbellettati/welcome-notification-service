package com.komoot.logic;

import com.komoot.entities.SignedUpNotificationEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class RecentUserCalculatorTest {

    @BeforeEach
    void setUp() {
    }

    @Test
    void checkRecentUser() throws ParseException {
        SignedUpNotificationEntity signup = new SignedUpNotificationEntity();
        signup.setName("User1");
        signup.setUserId(1);
        signup.setCreatedAt(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX").parse("2022-06-10T12:00:00+02"));

        Date start = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX").parse("2022-06-01T12:00:00+02");
        Date end = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX").parse("2022-06-03T12:00:00+02");
        assertNull(RecentUserCalculator.checkRecentUser(signup, start, end));

        start = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX").parse("2022-06-01T12:00:00+02");
        end = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX").parse("2022-06-11T12:00:00+02");
        assertEquals(signup, RecentUserCalculator.checkRecentUser(signup, start, end));

        start = null;
        end = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX").parse("2022-06-11T12:00:00+02");
        assertEquals(signup, RecentUserCalculator.checkRecentUser(signup, start, end));

        start = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX").parse("2022-06-01T12:00:00+02");
        end = null;
        assertEquals(signup, RecentUserCalculator.checkRecentUser(signup, start, end));

        start = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX").parse("2022-06-12T12:00:00+02");
        end = null;
        assertNull(RecentUserCalculator.checkRecentUser(signup, start, end));

        start = null;
        end = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX").parse("2022-06-09T12:00:00+02");;
        assertNull(RecentUserCalculator.checkRecentUser(signup, start, end));
    }
}