docker image rm com.komoot.notification-service:$1
./gradlew build  -x test && \
docker build -t com.komoot.notification-service:$1 -f src/main/docker/Dockerfile .

docker tag com.komoot.notification-service:$1 gnagno91/com.komoot.notification-service:$1
docker push gnagno91/com.komoot.notification-service:$1